var express = require('express');
var controller_javascript_file = require('./controllers/controller_javascript_file');
var app = express();
var bodyparser = require('body-parser');

//set up template engine
app.set('view engine' , 'ejs');


//static files for middleware
app.use(express.static('./public'));
app.use(bodyparser.urlencoded({extended: true}));


//fire controllers
controller_javascript_file(app);


//listen to port
app.listen(3000);
console.log('you are listening to port 3000');